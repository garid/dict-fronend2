// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

import Auth from './Auth.js'

import Axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'

import ElementUI from 'element-ui'
import locale from 'element-ui/lib/locale/lang/en'
import 'element-ui/lib/theme-default/index.css'


Vue.use(ElementUI, { locale })
Vue.use(VueRouter)
Vue.use(VueAxios, Axios)
Vue.use(Auth)


Axios.defaults.baseURL = 'http://localhost:8000/v1';

Axios.interceptors.request.use(function(response) {
  var token = Vue.auth.getToken();

  if (token) {
    response.headers['Authorization'] = `Token ${token}`;
  }
  
  return response;
}, function(error) {
  return Promise.reject(error);
});

Axios.interceptors.response.use(function (response) {
    // Do something with response data
    return response;
  }, function (error) {
    if (!error.response || error.response.status >= 500) {
      Notification.error({
        title: 'Алдаа',
        message: 'Хүсэлт явуулахад алдаа гарлаа'
      });
    }
    return Promise.reject(error);
  });

router.beforeEach(function(to, from, next) {

  if (to.matched.some(record => record.meta.requiresAuth) && !Vue.auth.loggedIn()) {
  
  } else {
    next();
  }
});

new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
