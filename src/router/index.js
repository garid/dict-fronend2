import Vue from 'vue'
import Router from 'vue-router'
/* import Hello from '@/components/Hello' */
import Home from '@/components/Home'
import Auth from '@/components/auth/Auth'
import Login from '@/components/auth/Login'
import Register from '@/components/auth/register'

import Words from '@/components/pages/words'
import About_Word from '@/components/pages/about_words'
import Offer_Word from '@/components/pages/offer_words'
import View_Offer_Word from '@/components/pages/view_offer_words'

import Add_words from '@/components/Add_words'
import Add_suggest from '@/components/Add_suggest'


Vue.use(Router)

export default new Router({
  routes: [
  {
    path: '/auth',
    component: Auth,
    children: [{
      path: 'login',
      component: Login
    }, {
      path: 'register',
      component: Register
    }]
  },
  {
    path: '/',
    component: Home,
  },
  {
    path: '/',
    component: Home,
    meta: {
      requiresAuth: true
    },
    children: [
    {
      path: 'words',
      component: Words,
    },
    {
      path: 'offer/:wordId',
      component: Offer_Word,
    },
    {
      path: 'word/:id',
      component: About_Word,
    },
    {
      path: 'view_offer',
      component: View_Offer_Word,
    },
    {
      path: 'add_words',
      component: Add_words,
    },],
  }]
})
